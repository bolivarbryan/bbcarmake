# Car Make & Models App #

This Demo is built in Swift 3.1

### Design Patterns used ###

* MVC
* Facade
* Decorator
* Adapter

### Models ###

* Make: id, name
* Car: model, make

### Unit Tests ###

* Models
* Controllers

### Cool features ###

* Pagination
* Core Data
* Autolayout
* NetWorking
* Toast Alerts

### About Developer ###

* Bryan Bolivar, iOS Dev since 2010
* LinkedIn: https://www.linkedin.com/in/bolivarbryan