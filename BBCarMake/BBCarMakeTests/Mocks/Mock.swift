//
//  Mock.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/5/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import Foundation

class Mock: NSObject {
    class func loadMock(fileName: String) -> Data? {
        guard let path = Bundle(for: type(of: self) as! AnyClass).path(forResource: fileName, ofType: "json") else {
            fatalError("\(fileName).json not found")
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            return try Data(contentsOf: url , options: Data.ReadingOptions.mappedIfSafe)
        }catch {
            print("Error converting")
            return nil
        }
    }
}
