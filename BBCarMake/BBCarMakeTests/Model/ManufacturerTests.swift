//
//  ManufacturerTests.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/3/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import XCTest
import SwiftyJSON

@testable import BBCarMake

class BrandTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testManufacturer_ShouldInit() {
        let manufacturer = Manufacturer()
        XCTAssertNotNil(manufacturer)
    }
    
    func testManufacturer_ShouldInitWithJSON() {
        //GIVEN
        let jsonDic = ["": ""]
        let json = JSON(jsonDic)
        
        //WHEN
        let manufacturer = Manufacturer(fromJSON: json)
        
        //THEN
        XCTAssertNotNil(manufacturer)
    }
    
    func testManufacturer_ShouldCopyFromJSON() {
        //GIVEN
        let expectedId = "101"
        let expectedName = "Bentley"
        
        //WHEN
        let jsonDic = ["id": expectedId, "manufacturer": expectedName]
        let json = JSON(jsonDic)
        
        //THEN
        var manufacturer = Manufacturer()
        manufacturer.copyFromJSON(json: json)
        
        XCTAssertEqual(manufacturer.name, expectedName)
        XCTAssertEqual(manufacturer.id, expectedId)
    }
    
}
