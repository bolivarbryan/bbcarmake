//
//  CarModelsViewController.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/6/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import UIKit

class CarModelsViewController: UIViewController {
    //MARK: - Properties
    var manufacturer: Manufacturer!
    var cars = [Car]()
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = manufacturer.name
        
        //Remove When Api response works
        loadFakeData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Functions
    func loadFakeData() {
        let car = Car(model: "CX-5", manufacturer: manufacturer)
        let car2 = Car(model: "BT-50", manufacturer: manufacturer)
        let car3 = Car(model: "Mazda2", manufacturer: manufacturer)
        
        cars.append(contentsOf: [car, car2, car3])
        tableView.reloadData()
    }
}

extension CarModelsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CarModelCellID"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        return configureCell(carCell: cell, car: cars[indexPath.row], indexPath: indexPath)
    }
    
    func configureCell(carCell: UITableViewCell, car: Car, indexPath: IndexPath) -> CarModelCell {
        let cell = carCell as! CarModelCell
        
        cell.carModelNameLabel.text = car.model
        //finding favorite Car Models
        if (LibraryAPI.sharedInstance.getCars().contains { carObject in
            //TODO: Integrate Equatable Protocol
            return car.model == carObject.model }) == true {
            cell.accessoryType = .checkmark
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
}

extension CarModelsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let car = cars[indexPath.row]
        AlertHelper.sharedInstance.showAlertInController(controller: self, message: "Model: \(car.model) \n Manufacturer: \(car.manufacturer.name)")
        
        // save car to favorites
        LibraryAPI.sharedInstance.addCar(car: car)
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
         
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        LibraryAPI.sharedInstance.deleteCar(car: cars[indexPath.row])
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
}
