//
//  ManufacturersViewController.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/6/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import UIKit

class ManufacturersViewController: UIViewController {
    
    //MARK: - Properties
    fileprivate var manufacturers = [Manufacturer]()
    @IBOutlet weak var tableView: UITableView!
    fileprivate var page = 0
    fileprivate var totalPageCount = 0
    fileprivate let pageSize = 15
    var selectedIndex: Int = 0
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 64
        loadManufacturers()
        
        //TODO: comment this line when api response works
        loadFakeData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ModelsSegue" {
            let vc = segue.destination as! CarModelsViewController
            vc.manufacturer = manufacturers[selectedIndex]
        }
    }
    
    //MARK: Functions
    
    func loadFakeData() {
        let manufacturer = Manufacturer(id: "101", name: "Mazda")
        manufacturers.append(manufacturer)
    }
    
    func loadManufacturers() {
        LibraryAPI.sharedInstance.listManufacturers(pageCount: pageSize, pageNumber: page) { (manufacturers, page, totalPageCount) in
            self.page = page + 1
            self.manufacturers.append(contentsOf: manufacturers)
            self.tableView.reloadData()
        }
    }
}

extension ManufacturersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ManufacturerCellID"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ManufacturerCell
        cell.manufacturerNameLabel.text = manufacturers[indexPath.row].name
        
        if indexPath.row == manufacturers.count - 1 { // Pagination
            loadManufacturers()
        }
        
        return cell
    }
}

extension ManufacturersViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return manufacturers.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "ModelsSegue", sender: self)
    }
    
}
