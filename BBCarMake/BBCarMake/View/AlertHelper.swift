//
//  AlertHelper.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/6/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import UIKit
import Toaster


class AlertHelper: NSObject {
    //MARK: - Singleton
    
    static let sharedInstance = AlertHelper()
    
    
    //MARK: - Initializers
    override init() {
        super.init()
    }
    
    //MARK: - Public
    
    func showToastMessage(message: String) {
        Toast(text: message).show()
    }
    
    func showAlertInController(controller: UIViewController, message: String) {
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            
        let OKAction = UIAlertAction(title: "Ok", style: .default) { action in
        }
        
        alertController.addAction(OKAction)
            controller.present(alertController, animated: true) { }
        }
    }
}
