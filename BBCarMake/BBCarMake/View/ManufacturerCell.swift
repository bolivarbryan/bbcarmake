//
//  ManufacturerCell.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/6/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import UIKit

class ManufacturerCell: UITableViewCell {

    //MARK: - Properties
    
    @IBOutlet weak var manufacturerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
