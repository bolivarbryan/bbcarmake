//
//  HTTPClient.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/6/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct HTTPClient {
    
    func getRequest(url: String, completion: @escaping (_ response: JSON) -> Void) {
        Alamofire.request(url).responseJSON { response in
            guard let data = response.data else {
                return
            }
            
            if response.response?.statusCode != 200 { // Show toast message for error
                AlertHelper.sharedInstance.showToastMessage(message: "Error Loading Data\nStatus Code: \(response.response!.statusCode)")
            }
            
            let json = JSON(data: data)
            completion(json)
        }
    }
}
