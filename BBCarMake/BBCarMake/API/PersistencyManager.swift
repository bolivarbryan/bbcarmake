//
//  PersistencyManager.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/6/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import Foundation

class PersistencyManager: NSObject {
    
    //MARK: - Properties
    
    private var cars = [Car]()
    
    //MARK: - Initializers
    
    override init() {
    }
    
    //MARK: - Public
    
    func getCars() ->  [Car] {
        return cars
    }
    
    func addCar(car: Car) {
        cars.append(car)
    }
    
    func deleteCarAtIndex(car: Car) {
        
       let indexOfCar = cars.index { (carObject) -> Bool in
            //TODO: Include Equatable Protocol
            return (carObject.model == car.model)
        }
        if let index = indexOfCar {
            cars.remove(at: index)
        }
        
    }
}
