//
//  LibraryAPI.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/5/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import UIKit
import SwiftyJSON

class LibraryAPI: NSObject {
    //MARK: - Singleton
    static let sharedInstance = LibraryAPI()
    
    //MARK: Properties
    private let persistencyManager: PersistencyManager
    private let httpClient: HTTPClient
    private let isOnline: Bool
    
    //MARK: - Initializer
    override init() {
        persistencyManager = PersistencyManager()
        httpClient = HTTPClient()
        isOnline = false
        
        super.init()
    }
    
    //MARK - Public
    func listManufacturers(pageCount: Int, pageNumber:Int, completion: @escaping (_ manufacturers: [Manufacturer], _ totalPageCount:Int, _ page: Int) ->  Void) {
        let pageAndCountparameters = "&pageSize=\(pageCount)&page=\(pageNumber)"
        httpClient.getRequest(url: APIEndpoint.manufacturersURL + pageAndCountparameters ) { (json) in
            
            //List of Manufacturers ["id": "name"]
            guard let wkda = json.dictionaryObject?["wkda"] as? [String: String] else {
                completion([], pageNumber, 0) // no response from server
                return
            }
            
            let manufacturers = wkda.map { (object) -> Manufacturer in
                let dic = ["id": object.key, "manufacturer": object.value]
                let json = JSON(dic)
                return Manufacturer(fromJSON: json)
            } // This map transform the gotten response to a readable Model
            
            let page = json.dictionary?["page"]?.intValue // current page
            let totalPageCount = json.dictionary?["page"]?.intValue // total pages
            
            completion(manufacturers, page!, totalPageCount!)
        }
    }
    
    func listCarModelsFromManufacturer(manufacturer: Manufacturer, completion:(_ cars: [Car]) -> Void) {
        httpClient.getRequest(url: APIEndpoint.carModelsURL + "&manufacturer=\(manufacturer.id)") { (json) in
            //TODO: Parse Response
        }
    }
    
    func getCars() -> [Car] {
        return persistencyManager.getCars()
    }
    
    func addCar(car: Car) {
        persistencyManager.addCar(car: car)
    }
    
    func deleteCar(car: Car) {
        persistencyManager.deleteCarAtIndex(car: car)
    }
}
