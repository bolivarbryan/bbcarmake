//
//  Constants.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/6/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import Foundation

struct APIEndpoint {
    private static let serverURL = "http://api-aws-eu-qa-1.auto1-test.com"
    private static let apiToken = "API_TOKEN_HERE" //TODO: Insert Api Token
    static let manufacturersURL = APIEndpoint.serverURL + "/v1/car-types/manufacturer" + "?wa_key=\(APIEndpoint.apiToken)"
    static let carModelsURL = APIEndpoint.serverURL + "/v1/car-types/main-types" + "?wa_key=\(APIEndpoint.apiToken)"
}
