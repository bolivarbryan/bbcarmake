//
//  Car.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/4/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import Foundation

struct Car {
    //MARK: - Properties
    var model: String
    var manufacturer: Manufacturer
    
    
    //MARK: - Initializers
    
    init(model:String, manufacturer: Manufacturer) {
        self.model = model
        self.manufacturer = manufacturer
    }
}
