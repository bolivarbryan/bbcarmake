//
//  Make.swift
//  BBCarMake
//
//  Created by Bryan A Bolivar M on 5/3/17.
//  Copyright © 2017 Bryan A Bolivar M. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Manufacturer {
    //MARK: - Properties
    var id: String = ""
    var name: String = ""
    
    //MARK: Initializers
    init() {
        //DO NOTHING
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(fromJSON: JSON) {
        copyFromJSON(json: fromJSON)
    }
    
    // MARK: - Public
    
    mutating func copyFromJSON(json: JSON) {
        id = json["id"].stringValue
        name = json["manufacturer"].stringValue
    }
   
}
